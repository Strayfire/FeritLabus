package hr.ferit.bruno.labusferit.AccelerationSensor.Presenter;

import hr.ferit.bruno.labusferit.AccelerationSensor.Model.AccelerationCalculator;
import hr.ferit.bruno.labusferit.AccelerationSensor.Model.AndroidAccelerationSensor;
import hr.ferit.bruno.labusferit.AccelerationSensor.View.IAccelerationView;
import hr.ferit.bruno.labusferit.Common.ISensor;

/**
 * Created by Stray on 23.12.2017..
 */

public class AccelerationPresenter implements IAccelerationPresenter {

	IAccelerationView mView;
	ISensor mAccelerationSensor;

	private static final double ACCELERATION_THRESHOLD = 10.00;
	private static final double SOUND_THRESHOLD= 12.00;

	public AccelerationPresenter(IAccelerationView view) {
		mView = view;
		try{
			mAccelerationSensor = new AndroidAccelerationSensor(this);
		}
		catch (RuntimeException exception){
			mView.displayDialog(exception.getMessage());
		}
	}

	@Override
	public void startTracking() {
		if (mAccelerationSensor != null)
			mAccelerationSensor.startTracking();
	}

	@Override
	public void stopTracking() {
		if (mAccelerationSensor != null)
			mAccelerationSensor.stopTracking();
	}

	@Override
	public void onSensorMeasurmentChanged(float[] values) {
		double total = AccelerationCalculator.accelerationFromComponents(values);
		mView.displayTotal(total);
		mView.displayXAcc(values[0]);
		mView.displayYAcc(values[1]);
		mView.displayZAcc(values[2]);
		if(total > ACCELERATION_THRESHOLD){
			mView.excessAcceleration();
			if(total>SOUND_THRESHOLD){
				mView.playSound();
			}
		}else{
			mView.normalAcceleration();
		}
	}
}

package hr.ferit.bruno.labusferit.AccelerationSensor.View;

/**
 * Created by Zoric on 22.12.2017..
 */

public interface IAccelerationView {
    public void excessAcceleration();
    public void normalAcceleration();
    public void displayXAcc(double xAcc);
    public void displayYAcc(double yAcc);
    public void displayZAcc(double zAcc);
    public void displayTotal(double totalAcc);
    public void playSound();
    void displayDialog(String message);
}

package hr.ferit.bruno.labusferit.AccelerationSensor.Presenter;

import hr.ferit.bruno.labusferit.Common.ISensorChangeListener;

/**
 * Created by Zoric on 22.12.2017..
 */

public interface IAccelerationPresenter extends ISensorChangeListener {
	public void startTracking();
	public void stopTracking();
}

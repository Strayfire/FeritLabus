package hr.ferit.bruno.labusferit.Utilities;

import java.text.DecimalFormat;

/**
 * Created by Zoric on 22.12.2017..
 */

public class Formatting {
    public static String DECIMAL_FORMAT = "00.00";

    public static String doubleToTwoDecimalPlaces (double value){
        DecimalFormat format = new DecimalFormat(DECIMAL_FORMAT);
        return format.format(value);
    }
}

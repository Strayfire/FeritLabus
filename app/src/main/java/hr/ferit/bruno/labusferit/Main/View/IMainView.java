package hr.ferit.bruno.labusferit.Main.View;

/**
 * Created by Zoric on 21.12.2017..
 */

public interface IMainView {
    public void goToLightSensor();
    void goToAccelerationSensor();
    void goToMagneticSensor();
}
